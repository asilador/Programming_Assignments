import numpy as np
import numpy
from scipy.optimize import minimize

# Import text files 
Q = np.asmatrix(np.loadtxt('Q.txt'))
b = np.asmatrix(np.loadtxt('b.txt'))
c = np.asmatrix(np.loadtxt('c.txt'))
b = np.transpose(b) #make b a column vector
m = 1
x_c = np.transpose(np.asmatrix(np.zeros(np.size(b)))*1)

# Define f(x)
def f(Q,b,c,x):	
	if np.shape(x) == (5L,1L):
		ans = np.transpose(x)*Q*x + np.transpose(b)*x + c
	else:
		
		x = np.transpose(np.asmatrix(x))
		ans =  np.transpose(x)*Q*x + np.transpose(b)*x + c
	return ans
	
# Define gradient f(x)
def gradf(Q,b,x):
	return 2*Q*x + b

# Define algorithm for Armijos rule
def armijo(alpha0,Q,b,c,D,m,x):
	
	alpha = alpha0
	#print('alpha is ', alpha)
	s = 1.0
	sigma = 1.0
	beta = 1.0/3
	while f(Q,b,c,x+np.transpose(beta**m*s*D)) < f(Q,b,c,x) + sigma*beta**m*s*(np.transpose(gradf(Q,b,x)))*np.transpose(D):
		m+=1
		#print('m is ', m)
		alpha = beta**m*s
		#print('alpha is ', alpha)
	return alpha
			
# Begin Gradient Descent Algorithm
def grad_opt(epsilon,x,count,alpha):
	#print('alpha is ', alpha)
	while np.linalg.norm(gradf(Q,b,x))>= epsilon:
		D = -1*np.transpose(gradf(Q,b,x))/np.linalg.norm(gradf(Q,b,x))
		#print('D is ', D)
		alpha = armijo(alpha,Q,b,c,D,m,x)
		#print('alpha0 is ', alpha)
		count += 1
		if count%1000==0:
			print 'f(x) is ', f(Q,b,c,x)
			
		#print('norm of gradf(x) is ', np.linalg.norm(gradf(Q,b,x)))
		x -= alpha*gradf(Q,b,x)
		
				
	print '\nDone at ', count,'th iteration'
	print 'x* is ', x
	print 'f(x*) is ', f(Q,b,c,x)
	print 'epsilon is ', epsilon
	return 0

#part b
#gradient of f * x = zero vector. solve for x by getting inverse of grad_f
# grad f = 2*Q*x + b 
#setting to zero gives us 2*Q*x = -b
def partb(Q,b,x):
	Qinv = np.linalg.inv(2*Q)
	ans = Qinv*(-b)
	print '\n Solving for x* using the inverse of 2*Q*x=-b is', ans
	print f(Q,b,c,x)
	print 'f(x*) is', f(Q,b,c,ans)
	return 0

class partcfun:
	def __init__(self,arg1):
		self.arg1 = arg1
	def __call__(self,arg1):
		return f(Q,b,c,arg1)

def partc(x_0c):	
	cans = minimize(partcfun(x_c),x_0c)
	print 'x is', cans
	return 0
	
def run(epsilon):
	# Make a guess for x vector
	#x = np.asmatrix(np.zeros(np.size(b)))
	#x = np.transpose(x) #make column vector 
	#x=np.matrix(np.random.rand(5,1))	
	x = np.transpose(np.asmatrix(np.ones(np.size(b)))*1)
	count = 0
	alpha0 = 1.0
	print '******** Part A ********'
	grad_opt(epsilon,x,count,alpha0)
	print '******** Part B ********'
	partb(Q,b,x)
	print '******** Part C ********'
	x_0c = np.transpose(np.asmatrix(np.ones(np.size(b)))*1)
	partc(x_0c)
	return 0 
	
run(0.00001)